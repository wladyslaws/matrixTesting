import PackageDescription

let package = Package(
    name: "testupsurge",
    targets: [],
    dependencies: [
        .Package(url: "https://github.com/aleph7/Upsurge.git", Version(0,8,.max)),
    ]
)
