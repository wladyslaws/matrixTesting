//
//  ViewController.swift
//  testupsurge
//
//  Created by Wladyslaw Surala on 19/09/2017.
//  Copyright © 2017 Wladyslaw Surala. All rights reserved.
//

import UIKit
import Upsurge

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let length = 1024

//        let a = ValueArray<Double>(capacity:1024)
//        let b = ValueArray<Double>(capacity:1024)
//
//        for _ in 1...length {
//            a.append(Double(arc4random()))
//            b.append(Double(arc4random()))
//        }

        let a = Matrix<Float32>(rows: length,columns:length)
        let b = Matrix<Float32>(rows: length,columns:length)

        for i in 0..<length {
            for j in 0..<length {
                a[[i,j]] = Float32(arc4random())
                b[[i,j]] = Float32(arc4random())
            }

        }

        let start = Date()
        let result = a * b
//        let result = a + b
//        let result = a • b

        let end = Date()

        print("time: \(end.timeIntervalSince(start)) s")
        print("\(result[[102,506]])")
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

